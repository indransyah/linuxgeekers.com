<?php

class AdminPageController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function index() {
		$pages = Page::all();
		$this->layout->content = View::make('backend.page.index')
			->with('pages', $pages);
	}

	public function create() {
		$this->layout->content = View::make('backend.page.create');
	}

	public function store() {
		$validator = Validator::make(Input::all(), Page::$rules);
		if ($validator->passes()) {
			$page = new Page;
			$page->title = Input::get('title');
			$page->slug = Str::slug(Input::get('title'), '-');
			$page->content = Input::get('content');
			$page->user_id = Sentry::getUser()->id;
			$page->save();
			return Redirect::action('AdminPageController@index')
				->with('success', 'Page successfully added!');
		} else {
			return Redirect::action('AdminPageController@create')
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
		}
	}

	public function edit($id) {
		$page = Page::find($id);
		if ($page) {
			$this->layout->content = View::make('backend.page.edit')
				->with('page', $page);
		} else {
			return Redirect::action('AdminPageController@index');
		}
	}

	public function update($id) {
		$rules = Page::$rules;
        $rules['title'] .= ',' . $id . ',id';
		$validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
        	// return Input::all();
        	$page = Page::find($id);
			$page->title = Input::get('title');
			$page->slug = Str::slug(Input::get('title'), '-');
			$page->content = Input::get('content');
            $page->save();
			return Redirect::action('AdminPageController@index')
				->with('success', 'Page successfully edited!');
        } else {
        	return Redirect::action('AdminPageController@edit', $id)
				->with('error', 'The following errors occurred')
				->withErrors($validator)
				->withInput();
        }
	}

	public function destroy($id) {
		$page = Page::find($id);
		if ($page) {
			$page->delete();
			return Redirect::action('AdminPageController@index')
				->with('success', 'Page successfully deleted!');
		} else {
			return Redirect::action('AdminPageController@index');

		}
	}

	public function link($id) {
		$page = Page::find($id);
		$page->featured = 1;
		$page->save();
		return Redirect::action('AdminPageController@index')
			->with('success', 'Successfully pin the page as navigation menu');
	}

	public function unlink($id) {
		$page = Page::find($id);
		$page->featured = 0;
		$page->save();
		return Redirect::action('AdminPageController@index')
			->with('success', 'Successfully unpin the page as navigation menu');
	}

}
