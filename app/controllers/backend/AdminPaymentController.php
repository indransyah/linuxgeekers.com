<?php

class AdminPaymentController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		$payments = Payment::whereHas('invoice', function($q) {
                $q->where('status', 1); 
            })->get();
		// return $payment;
		$this->layout->content = View::make('backend.payment.index')
			->with('payments', $payments);
	}

	public function getShow($id) {
		// $order = Order::find($id);
		$payment = Payment::find($id);
		$this->layout->content = View::make('backend.payment.show')
			->with('payment', $payment);
			// ->with('order', $order);
	}

	public function getReset($id) {
		$payment = Payment::find($id);
		$payment->status = 'belum lunas';
		$payment->amount = 0;
		$payment->save();
		return Redirect::action('AdminPaymentController@getShow', $id)->with('success', 'Successfully reset the payment status');
	}

	public function postUpdate($id) {
		$payment = Payment::find($id);
		$payment->status = Input::get('status');
		$payment->save();
		return Redirect::action('AdminPaymentController@getShow', $id)->with('success', 'Successfully change the payment status');
	}

	public function deleteDestroy($id) {
		$payment = Payment::find($id);
		$payment->invoice->status = 0;
		$payment->invoice->save();
		$payment->delete();
		return Redirect::action('AdminPaymentController@getIndex')->with('success', 'Payment was deleted');
	}






	public function postRollback($id) {
		$payment = Payment::find($id);
		$payment->status = 'belum lunas';
		$payment->save();
		return Redirect::action('AdminPaymentController@getShow', $id)->with('success', 'Payment was rolled back');
	}

}
