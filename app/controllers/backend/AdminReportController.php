<?php

class AdminReportController extends BaseController {

	protected $layout = 'backend.layouts.master';

	public function getIndex() {
		$this->layout->content = View::make('backend.report.index');
	}

	public function getReport() {
		return $this->getIndex();
	}

	public function postReport() {
		// return Input::all();
		$range = explode(' - ', Input::get('reservationtime'));
		// return $range;
		$orders = Order::whereBetween('date', $range)->whereHas('shipment', function($q) {
			$q->where('status', 'dikirim');
		})->get();
		// $omset = $orders->sum('total');
		// return $omset;
		$pdf = PDF::loadView('backend.pdf.orders', compact('orders', 'range'));
		return $pdf->stream();
	}

	public function getDateTime() {
		return Order::find(3)->date;
	}

}
