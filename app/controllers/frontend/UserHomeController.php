<?php

class UserHomeController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex() {
		$this->layout->content = View::make('frontend.home');
	}

	public function getHome() {
		$this->getIndex();
	}

}
