<?php

class UserOrderController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function __construct()
	{
        $this->beforeFilter('customer', array('only' => array('getCheckout', 'postCheckout')));
    }

	public function getIndex()
	{
		$this->getCart();
	}

	public function getCart() {
		$cart = Cart::content();
		// return $cart;
		$this->layout->content = View::make('frontend.order.cart');
	}

	public function getClear() {
		Cart::destroy();
		return Redirect::action('UserProductController@getIndex');
		
	}

	public function postAdd($slug) {
		$product = Product::where('slug', $slug)->with('picture')->first();
		// Cart::add($product->id, $product->name, 1, $product->price, array('size' => Input::get('size')));
		Cart::associate('Product')->add($product->id, $product->name, 1, $product->price, array('size' => Input::get('size')));
		return Redirect::action('UserOrderController@getCart');
	}

	public function getCheckout() {
		$cart = Cart::content();
		$this->layout->content = View::make('frontend.order.cart');
	}

	public function postCheckout() {

	}

}
