<?php

class UserPageController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex($slug)
	{
		$page = Page::where('slug', '=', $slug)->first();
		$this->layout->content = View::make('frontend.page')
			->with('page', $page);
	}

}
