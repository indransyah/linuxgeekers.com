<?php

class UserProductController extends BaseController {

	protected $layout = 'frontend.layouts.master';

	public function getIndex() {
		$products = Product::published()->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

	public function getPreOrder() {
		$products = Product::published()->where('system', 'pre order')->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

	public function getReadyStock() {
		$products = Product::published()->where('system', 'ready stock')->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

	public function getShow($slug) {
		$product = Product::published()->where('slug', $slug)->with('picture')->first();
		$this->layout->content = View::make('frontend.product.show')
			->with('product', $product);
	}

	public function getSearch() {

	}

	public function postSearch() {
		$products = Product::published()->where('name', 'like', '%'.Input::get('keyword').'%')->with(array('picture' => function($query) {
			$query->first();
		}))->get();
		$this->layout->content = View::make('frontend.product.index')
			->with('products', $products);
	}

}
