<?php

class CompanyTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('company')->delete();
		Company::create(array(
			'name' => 'Linux Geekers',
			'description' => 'Best Linux Apparel',
			'address' => 'Jl.Imogiri Barat Km 12 Bantul Yogyakarta',
			'phone' => '085729009963',
			'email' => 'admin@linuxgeekers.com',
			'bbm' => '7CB2DABD',
			'facebook' => 'linux.geekers',
			'twitter' => 'linuxgeekers'
		));
	}

}
