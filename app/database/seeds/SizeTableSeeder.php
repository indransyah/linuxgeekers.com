<?php

class SizeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('sizes')->delete();
		Size::create(array(
			'name' => 'S',
			'value' => 's',
		));
		Size::create(array(
			'name' => 'M',
			'value' => 'm',
		));
		Size::create(array(
			'name' => 'L',
			'value' => 'l',
		));
		Size::create(array(
			'name' => 'XL',
			'value' => 'xl',
		));
		Size::create(array(
			'name' => 'XXL',
			'value' => 'xxl',
		));
		Size::create(array(
			'name' => 'XXXL',
			'value' => 'xxxl',
		));
	}

}
