<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();
		
		// Create Admin
		$user = Sentry::createUser(array(
			'name'		=> 'Administrator',
			'email'     => 'admin@linuxgeekers.com',
			'password'  => 'admin',
			'activated' => true,
			));

    	// Find the group using the group id
		$administratorGroup = Sentry::findGroupByName('Administrator');
		$customerGroup = Sentry::findGroupByName('Customer');

    	// Assign the group to the user
		$user->addGroup($administratorGroup);
		$user->addGroup($customerGroup);

		// Create Customer
		$user = Sentry::createUser(array(
			'name'		=> 'Indransyah',
			'email'     => 'indransyah@gmail.com',
			'password'  => 'customer',
			'activated' => true,
			));

    	// Find the group using the group id
		$customerGroup = Sentry::findGroupByName('Customer');

    	// Assign the group to the user
		$user->addGroup($customerGroup);

	}

}
