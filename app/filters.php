<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

// Route::filter('auth', function()
// {
// 	if (Auth::guest())
// 	{
// 		if (Request::ajax())
// 		{
// 			return Response::make('Unauthorized', 401);
// 		}
// 		else
// 		{
// 			return Redirect::guest('adidev/user/login');
// 		}
// 	}
// });


// Route::filter('auth.basic', function()
// {
// 	return Auth::basic();
// });

Route::filter('auth', function()
{
	if (!Sentry::check()) return Redirect::guest('member/login');
});

Route::filter('administrator', function()
{
	// if (!Sentry::check()) return Redirect::Action('AdminUserController@getLogin');
	if (!Sentry::check()) {
		// Session::put('redirect', URL::current());
		return Redirect::guest(URL::action('AdminUserController@getLogin'))->with('error', 'Please login first!');
	}
	
	if (!Sentry::getUser()->hasAccess('administrator')) return Redirect::Action('AdminUserController@getLogin');

	// $user = Sentry::getUser();
	// $admin = Sentry::findGroupByName('Administrator');
	// if (!$user->inGroup($admin)) return Redirect::Action('AdminUserController@getLogin');
});

Route::filter('customer', function()
{
	// if (!Sentry::check()) return Redirect::guest('login');
	if (!Sentry::check()) {
		return Redirect::guest('member/login')->with('error', 'Please login first!');
	}
	if (!Sentry::getUser()->hasAccess('customer')) return Redirect::to('member/login');
	
	// $user = Sentry::getUser();
	// $users = Sentry::findGroupByName('Customer');
	// if (!$user->inGroup($users)) return Redirect::to('login');
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
