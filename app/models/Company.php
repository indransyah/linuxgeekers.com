<?php

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Company extends Eloquent implements StaplerableInterface {
    use EloquentTrait;

    protected $table = 'company';

    protected $guarded = array('id');

    public $timestamps = true;

    public static $rules = array(
        'name' => 'required|max:50',
        'email' =>'required|email|max:30',
        'description' =>'required',
        'address' =>'required', 
        'phone' => 'required|digits_between:11,12',
    );

    // Add the 'avatar' attachment to the fillable array so that it's mass-assignable on this model.
    // protected $fillable = ['avatar', 'first_name', 'last_name'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('logo', [
            'styles' => [
                'medium' => '250x250',
                'thumb' => '150x150'
            ],
            'url' => '/images/:attachment/:id_partition/:style/:filename',
            'default_url' => '/images/default-logo.jpg'
        ]);

        parent::__construct($attributes);
    }

    public function scopeDesc($query) {
        return $query->select('description')->first();
    }

    public function scopeSocial($query) {
        return $query->select('facebook', 'twitter')->first();
    }

    public function scopeContact($query) {
        return $query->select('address', 'phone', 'email', 'bbm')->first();
    }
}