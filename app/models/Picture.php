<?php

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Picture extends Eloquent implements StaplerableInterface {
    use EloquentTrait;

    protected $table = 'pictures';

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('photo', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ],
            'url' => '/images/:attachment/:id_partition/:style/:filename',
            'default_url' => '/images/default-product.jpg'
        ]);

        parent::__construct($attributes);
    }

    public function product() {
		return $this->belongsTo('Product');
	}

    // public function scopeFirst($query, $product_id) {
    //     $count = $query->where('product_id', $product_id)->count();
    //     if ($count > 0) {
    //         return $query->where('product_id', $product_id)->photo->first()->get();
    //     } else {
    //         return URL::to('images/default-product.jpg');
    //     }
    // }
}