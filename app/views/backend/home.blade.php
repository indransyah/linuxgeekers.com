@extends('backend.layouts.master')
@section('content')
	<section class="content">
		@include('backend.layouts.alert')
		@if (Session::has('success'))
		<!-- <div class="alert alert-success alert-dismissable">
			<i class="fa fa-check"></i>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<b>Welcome {{ Sentry::getUser()->name }}!</b> {{ Session::get('success') }}
		</div> -->
		@endif
	</section>
@stop