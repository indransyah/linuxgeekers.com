        <style type="text/css">
        .dropdown-menu > li > a:hover {
            background-color: #999; 
            color: #F9F9F9; 
        }
        </style>
        <!-- header logo: style can be found in header.less -->
        <header class="header">
                <a class="logo" href="{{ URL::action('UserHomeController@getIndex') }}"><img src="{{ Company::find(1)->logo->url('thumb') }}" alt=""/></a>
            <!-- <a href="{{ URL::action('UserHomeController@getIndex') }}" class="logo">
                Add the class icon to your logo image or logo icon to add the margining
                Linux Geekers
            </a> -->
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Sentry::getUser()->name }} <b class="caret"></b></a>
                            <ul class="dropdown-menu square info no-border margin-list-rounded with-triangle">
                                <li> 
                                    <a href="{{ URL::action('AdminUserController@getProfile') }}">Profile</a>
                                </li>
                                <li> 
                                    <a href="{{ URL::action('AdminUserController@getLogout') }}">Logout</a>
                                </li>
                            </ul>
                        </li>
                        
                        <!-- User Account: style can be found in dropdown.less -->
                        <!-- <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{ Sentry::getUser()->name }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header bg-light-blue">
                                    <img src="img/avatar3.png" class="img-circle" alt="User Image" />
                                    <p>
                                        Jane Doe - Web Developer
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ URL::action('AdminUserController@getProfile') }}" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ URL::action('AdminUserController@getLogout') }}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
            </nav>
        </header>