@extends('backend.layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Add Page
        <!-- <small>Control panel</small> -->
    </h1>
</section>
<!-- Main content -->
<section class="content">
    @include('backend.layouts.alert')
    <div class='row'>
        <div class='col-md-12'>
            <div class='box'>
                <div class='box-body pad'>
                    {{ Form::open(array('action' => 'AdminPageController@store')) }}
                    <div class="form-group">
                        <label>Title</label>
                        {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Page title', 'required' => 'true' )) }}
                    </div>
                    {{ Form::textarea('content', null, array('rows' => '10', 'cols' => '80' )) }}
                    <div class="box-footer clearfix">
                        <button class="pull-right btn btn-info btn-flat" type="submit">Add <i class="fa fa-plus"></i></button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div><!-- /.box -->
        </div><!-- /.col-->
    </div><!-- ./row -->
</section><!-- /.content -->
<script type="text/javascript">
$(function() {
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('content');
                        //bootstrap WYSIHTML5 - text editor
                        $(".textarea").wysihtml5();
                    });
</script>
@stop()