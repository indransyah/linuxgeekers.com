@extends('backend.layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Change Password
	</h1>

</section>
<!-- Main content -->
<section class="content">
	@include('backend.layouts.alert')

	<div class="row">
		<div class="col-lg-6">
			{{ Form::open(array('action'=>'AdminUserController@postPassword', 'class'=>'form-horizontal')) }}
			<div class="form-group">
				<label class="col-sm-4 control-label">Current Password</label>
				<div class="col-sm-8">
					{{ Form::password('current_password', array('class'=>'form-control', 'required'=>'true')) }}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">New Password</label>
				<div class="col-sm-8">
					{{ Form::password('password', array('class'=>'form-control', 'required'=>'true')) }}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">Confirm Password</label>
				<div class="col-sm-8">
					{{ Form::password('password_confirmation', array('class'=>'form-control', 'required'=>'true')) }}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-8">
					<button type="submit" class="btn btn-info btn-success btn-flat"><i class="glyphicon glyphicon-pencil"></i> Change</button>
				</div>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</section>
@stop