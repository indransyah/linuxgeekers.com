@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Payments
                        <!-- <small>Control panel</small> -->
                    </h1>
                    <!-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>
                    </ol> -->
                </section>
                <!-- Main content -->
                <section class="content">
                    @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Success!</b> {{ Session::get('success') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <!-- <div class="box-header">
                                    <h3 class="box-title">View Pages</h3>
                                </div> --><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Invoice</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($payments as $payment)
                                            <tr class="{{ ($payment->status == 'lunas') ? 'success' : 'danger' }}">
                                                <td>{{ strtoupper($payment->invoice->code) }}</td>
                                                <td>{{ $payment->invoice->order->user->name }}</td>
                                                <td>{{ Helpers::date($payment->date) }}</td>
                                                <td>{{ Helpers::rupiah($payment->amount) }}</td>
                                                <td>{{ strtoupper($payment->status) }}</td>
                                                <td>
                                                    <div class="btn-group"> 
                                                        <a href="{{ URL::action('AdminPaymentController@getShow', $payment->id) }}" class="btn btn-sm btn-info btn-flat"><i class="fa fa-fw fa-eye"></i></a>
                                                        <a class="btn btn-sm btn-danger btn-flat" data-toggle="modal" data-target="#deleteModal-{{ $payment->id }}" data-toggle="tooltip" data-placement="right" title="Delete order"><i class="fa fa-fw fa-trash-o"></i></a>
                                                        <div class="modal fade" id="deleteModal-{{ $payment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Are you sure to delete this payment from your database ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        {{ Form::open(array('action' => array('AdminPaymentController@deleteDestroy', $payment->id), 'method'=>'DELETE')) }}
                                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                                        {{ Form::close() }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <th>Rendering engine</th>
                                                <th>Browser</th>
                                                <th>Platform(s)</th>
                                                <th>Engine version</th>
                                                <th>CSS grade</th>
                                            </tr>
                                        </tfoot> -->
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop() 