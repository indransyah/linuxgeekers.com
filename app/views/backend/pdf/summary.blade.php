<html>
<head>
<title>Order Summary</title>
<style type="text/css">
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
  .no-margin {
    margin: 0px;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <h1>Order Detail</h1>
    <h3 class="no-margin">Name : {{ $product->name }}</h3>
    <h4 class="no-margin">SKU : {{ strtoupper($product->sku) }}</h4>
    <h4 class="no-margin">Order : {{ $detail->sum('total_qty') }}</h4>
    <h4 class="no-margin">Earning : {{ Helpers::rupiah($detail->sum('total')) }}</h4>
    <br>
    <strong>Product's Size Detail</strong>
    <table width="100%" class="outline-table">
      <tbody>
        <tr class="border-bottom border-right center">
          <td><strong>Size</strong></td>
          <td><strong>Total</strong></td>
        </tr>
        @foreach($detail as $row)
        <tr class="border-bottom border-right center">
          <td>{{ strtoupper($row->size) }}</td>
          <td>{{ $row->total_qty }}</td>
        </tr>
        @endforeach
        <tr>
          <td class="border-right center"><strong>Total</strong></td>
          <td class="center"><strong>{{ $detail->sum('total_qty') }}</strong></td>
        </tr>
      </tbody>
    </table>
    <br>
  </div>
</body>
</html>
