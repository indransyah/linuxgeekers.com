@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Products
                        <!-- <small>Control panel</small> -->
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content">
                    @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Success!</b> {{ Session::get('success') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Price</th>
                                                <th>Qty</th>
                                                <th>Order</th>
                                                <th width="10%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($products as $product)
                                            <tr>
                                                <td>{{ $product->id }}</td>
                                                <td>{{ $product->name }}</td>
                                                <td>{{ Helpers::rupiah($product->price) }}</td>
                                                <td>{{ $product->quota }}</td>
                                                <td>{{ $product->sum }}</td>
                                                <td>
                                                    <!-- <div class="btn-group"> -->
                                                        <a href="{{ URL::action('AdminPreOrderController@getShow', $product->id) }}" class="btn btn-sm btn-info"><i class="fa fa-fw fa-eye"></i></a>
                                                        <!-- <a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal-{{ $product->id }}" data-toggle="tooltip" data-placement="right" title="Delete page"><i class="fa fa-fw fa-trash-o"></i></a>
                                                        <div class="modal fade" id="deleteModal-{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Are you sure to delete <strong>{{ $product->name }}</strong> from your database ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        {{ Form::open(array('action' => array('AdminProductController@deleteDestroy', $product->id))) }}
                                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                                        {{ Form::close() }}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    <!-- </div> -->
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop() 