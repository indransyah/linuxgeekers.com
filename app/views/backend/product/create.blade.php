@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add Product
                        <!-- <small>Control panel</small> -->
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='box'>
                                <!-- /.box-header -->
                                <div class='box-body pad'>
                                    {{ Form::open(array('action' => 'AdminProductController@postStore', 'files' => 'true')) }}
                                        <div class="form-group">
                                            <label>Name</label>
                                            {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Product\'s name' )) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Price</label>
                                            {{ Form::text('price', null, array('class' => 'form-control', 'placeholder' => 'Product\'s price' )) }}
                                        </div>
                                       <!--  <div class="form-group">
                                            <label>Quantity</label>
                                            {{ Form::text('quota', null, array('class' => 'form-control', 'placeholder' => 'Product\'s quantity' )) }}
                                        </div> -->
                                        <div class="form-group">
                                            <label>Description</label>
                                            {{ Form::textarea('description', null, array('rows' => '10', 'cols' => '80' )) }}
                                        </div>
                                        <div class="form-group">
                                            <label>System</label>
                                            {{ Form::select('system', array('pre order' => 'Pre Order', 'ready stock' => 'Ready Stock'), null, array('class' => 'form-control')) }}
                                            <small><i>You cannot change the order system after save the product</i></small>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            {{ Form::select('status', array('avaliable' => 'Avaliable', 'draf' => 'Draf', 'sold out' => 'Sold Out'), null, array('class' => 'form-control')) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Photos</label>
                                            <br>
                                            <small><i>Save the product detail first to upload the product's photos!</i></small>
                                        </div>
                                        <div class="box-footer clearfix">
                                            <button class="pull-right btn btn-info btn-flat" type="submit">Add <i class="fa fa-plus"></i></button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div><!-- /.box -->
                        </div><!-- /.col-->
                    </div><!-- ./row -->

                </section><!-- /.content -->
                <script type="text/javascript">
                $(function() {
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('description');
                    });
                </script>
@stop()