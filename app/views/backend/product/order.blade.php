@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Orders <small>SKU : <strong>{{ strtoupper($product->sku) }}</strong> ({{ $product->name }})</small>
                        <span class="pull-right"><a href="{{ URL::action('AdminProductController@getCustomer', $product->id) }}" class="btn btn-info"><i class="fa fa-fw fa-users"></i> View Customers</a></span>
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-barcode"></i>
                    <h3 class="box-title">Product Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $product->name }}</dd>
                        <dt>SKU</dt>
                        <dd>{{ strtoupper($product->sku) }}</dd>
                        <dt>Qty</dt>
                        <dd>{{ $product->quota }}</dd>
                        <dt>Ordered</dt>
                        <dd>{{ $detail->sum('total_qty') }}</dd>
                        <dt>Total</dt>
                        <dd>{{ Helpers::rupiah($detail->sum('total')) }}</dd>
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        <div class="col-md-8">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-bars"></i>
                    <h3 class="box-title">Pre Order Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered text-center">
                        <tbody>
                            <tr>
                                <th>Size</th>
                                <th>Qty</th>
                            </tr>
                            @foreach($detail as $row)
                            <tr>
                                <td>{{ strtoupper($row->size) }} </td>
                                <td>{{ $row->total_qty }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td><strong>Total</strong></td>
                                <td><strong>{{ $detail->sum('total_qty') }}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <a target="_blank" href="{{ URL::action('AdminProductController@getSummary', $product->id) }}" class="btn btn-flat btn-info"><i class="fa fa-fw fa-download"></i> Download Summary</a>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        
    </div><!-- /.row -->
                </section><!-- /.content -->
@stop() 