@extends('backend.layouts.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Profile
	</h1>

</section>
<!-- Main content -->
<section class="content">
	@include('backend.layouts.alert')

	<div class="row">
		<div class="col-lg-6">
			{{ Form::open(array('action'=>'AdminUserController@postProfile', 'class'=>'form-horizontal')) }}
			<div class="form-group">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					{{ Form::text('name', Sentry::getUser()->name, array('class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true')) }}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					{{ Form::email('email', Sentry::getUser()->email, array('class'=>'form-control', 'placeholder'=>'Email', 'required'=>'true', 'id'=>'email')) }}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-flat btn-info"><i class="glyphicon glyphicon-pencil"></i> Change</button>
					<!-- {{ HTML::link('user/password', 'Change your password?', array('class'=>'pull-right')) }} -->
					<a href="{{ URL::action('AdminUserController@getPassword') }}" class="pull-right">Change your password?</a>
				</div>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</section>
@stop