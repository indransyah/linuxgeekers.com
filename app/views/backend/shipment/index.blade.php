@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Shipments
                        @if($packaging->count() > 0)
                        <span class="pull-right">
                            <a target="blank" href="{{ URL::action('AdminShipmentController@getBulkPrint') }}" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Print address of recipients when shipment's status is 'packaging'"><i class="fa fa-print"></i> Print Recipient's Address</a>
                        </span>
                        @endif
                        <!-- <small>Control panel</small> -->
                    </h1>
                    <!-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>
                    </ol> -->
                </section>
                <!-- Main content -->
                <section class="content">
                    @if (Session::has('success'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b>Success!</b> {{ Session::get('success') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <!-- <div class="box-header">
                                    <h3 class="box-title">View Pages</h3>
                                </div> --><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Invoice</th>
                                                <th>Name</th>
                                                <th>Courier</th>
                                                <th>Shipped to</th>
                                                <th width="7%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($shipments as $shipment)
                                            <tr class="{{ ($shipment->status == 'dikirim') ? 'success' : 'danger' }}">
                                                <td>{{ strtoupper($shipment->order->invoice->code) }}</td>
                                                <td>{{ $shipment->order->user->name }}</td>
                                                <td>{{ strtoupper($shipment->courier) }}</td>
                                                <td>{{ $shipment->recipient->city }}</td>
                                                <td>
                                                    <a href="{{ URL::action('AdminShipmentController@getShow', $shipment->id) }}" class="btn btn-sm btn-info"><i class="fa fa-fw fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <th>Rendering engine</th>
                                                <th>Browser</th>
                                                <th>Platform(s)</th>
                                                <th>Engine version</th>
                                                <th>CSS grade</th>
                                            </tr>
                                        </tfoot> -->
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
@stop() 