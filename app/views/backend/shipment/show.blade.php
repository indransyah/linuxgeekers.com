@extends('backend.layouts.master')
@section('content')
<style type="text/css">
.dl-horizontal dt {
    text-align: left;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Shipment Detail - <small>INVOICE : <strong><a href="{{ URL::action('AdminOrderController@getShow', $shipment->order->id) }}">{{ strtoupper($shipment->order->invoice->code) }}</a></strong></small><span class="pull-right" style="color: {{ ($shipment->status == 'dikirim') ? 'green' : 'red' }}">{{ strtoupper($shipment->status) }}</span>
    </h1>
    <!-- <div class="pull-right">
        <h3>LUNAS</h3>
    </div> -->
</section>

<!-- Main content -->
<section class="content">
    @include('backend.layouts.alert')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-credit-card"></i>
                    <h3 class="box-title">Shipment Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        {{ Form::open(array('action' => array('AdminShipmentController@postUpdate', $shipment->id))) }}
                        <dt>Courier</dt>
                        <dd>{{ strtoupper($shipment->courier) }}</dd>
                        <dt>Cost</dt>
                        <dd>{{ Helpers::rupiah($shipment->cost) }}</dd>
                        <dt>Message</dt>
                        <dd>{{ $shipment->message }}</dd>
                        <dt>Status</dt>
                        <dd>
                            {{ Form::select('status', array('belum dikirim' => 'Belum Dikirim', 'packaging' => 'Packaging', 'dikirim' => 'Dikirim', 'gagal' => 'Gagal'), $shipment->status, array('class' => 'form-control')) }}
                        </dd>
                        <br>
                        <dt>Tracking Number</dt>
                        <dd>{{ Form::text('tracking-number', $shipment->tracking_number, array('class' => 'form-control', 'type' => 'text' )) }}</dd>
                        <br>
                        <dt></dt>
                        <dd>
                            <button class="btn btn-info btn-flat" type="submit"><i class="fa fa-save"></i> Save</button>
                        </dd>
                        {{ Form::close() }}
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-usd"></i>
                    <h3 class="box-title">Payment Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Date</dt>
                        <dd>{{ Helpers::date($shipment->order->invoice->payment->date) }}</dd>
                        <dt>Amount</dt>
                        <dd>{{ Helpers::rupiah($shipment->order->invoice->payment->amount) }}</dd>
                        <dt>Status</dt>
                        <dd>{{ strtoupper($shipment->order->invoice->payment->status) }}</dd>
                        <br>
                        <dt></dt>
                        <dd>
                            <a href="{{ URL::action('AdminPaymentController@getShow', $shipment->order->invoice->payment->id) }}">More detail ...</a>
                        </dd>
                        </dd>
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        <div class="col-md-4">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-shopping-cart"></i>
                    <h3 class="box-title">Recipient Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Name</dt>
                        <dd>{{ $shipment->recipient->name }}</dd>
                        <dt>Phone</dt>
                        <dd>{{ $shipment->recipient->phone }}</dd>
                        <dt>Address</dt>
                        <dd>{{ $shipment->recipient->address }}</dd>
                        <dt>City</dt>
                        <dd>{{ $shipment->recipient->city }}</dd>
                        <dt>Province</dt>
                        <dd>{{ $shipment->recipient->province }}</dd>
                        <dt>Zip Code</dt>
                        <dd>{{ $shipment->recipient->zip_code }}</dd>
                        <br>
                        <dt></dt>
                        <dd>
                            <a href="{{ URL::action('AdminShipmentController@getPrint', $shipment->recipient->id) }}" target="_blank" class="btn btn-info btn-flat" type="submit"><i class="fa fa-print"></i> Print</a>
                        </dd>
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->
    </div>
</section>

@stop() 