@extends('backend.layouts.master')
@section('content')
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Add User
                        <!-- <small>Control panel</small> -->
                    </h1>
                    <!-- <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Blank page</li>
                    </ol> -->
                </section>
                <!-- Main content -->
                <section class="content">
                    @include('backend.layouts.alert')
                    <div class='row'>
                        <div class='col-md-6'>
                            <div class='box'>
                                <!-- <div class='box-header'>
                                    <h3 class='box-title'>CK Editor <small>Advanced and full of features</small></h3>
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-info btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div> -->
                                <!-- /.box-header -->
                                <div class='box-body pad'>
                                    {{ Form::open(array('action' => 'AdminUserController@postStore')) }}
                                        <div class="form-group">
                                            <label>Name</label>
                                            {{ Form::text('name', null, array('class' => 'form-control', 'required' => 'true', 'type' => 'text' )) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            {{ Form::text('email', null, array('class' => 'form-control', 'required' => 'true', 'type' => 'email' )) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            {{ Form::password('password', array('class' => 'form-control', 'required' => 'true', 'type' => 'password')) }}
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            {{ Form::password('password_confirmation', array('class' => 'form-control', 'required' => 'true', 'type' => 'password' )) }}
                                        </div>
                                        <div class="box-footer clearfix">
                                            <button class="pull-right btn btn-info btn-flat" type="submit">Add <i class="fa fa-plus"></i></button>
                                        </div>
                                        <!-- <button class="btn btn-success" type="submit">Add</button> -->
                                    {{ Form::close() }}
                                </div>
                            </div><!-- /.box -->

                            <!-- <div class='box'>
                                <div class='box-header'>
                                    <h3 class='box-title'>Bootstrap WYSIHTML5 <small>Simple and fast</small></h3>
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                </div>
                                <div class='box-body pad'>
                                    <form>
                                        <textarea class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </form>
                                </div>
                            </div> -->
                        </div><!-- /.col-->
                    </div><!-- ./row -->
                </section><!-- /.content -->
                <script type="text/javascript">
                $(function() {
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('content');
                        //bootstrap WYSIHTML5 - text editor
                        $(".textarea").wysihtml5();
                    });
                </script>
@stop()