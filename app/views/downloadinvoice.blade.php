<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>Invoice - Linux Geekers</title>
  <link href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css' rel='stylesheet' type='text/css' />

</head>
<body>
  <div class="container">
    <div class="row pad-top-botm "> 
      <div class="col-lg-6 col-md-6 col-sm-6 "> 
        <img src="" style="padding-bottom:20px;" /> 
        LINUX GEEKERS
      </div> 
      <div class="col-lg-6 col-md-6 col-sm-6">
        <strong>Linux Geekers</strong><br />
        <i>Address :</i> Jl.Imogiri Barat Km 12 Bantul<br />Yogyakarta 
      </div> 
    </div> 
    <div  class="row text-center contact-info"> 
      <div class="col-lg-12 col-md-12 col-sm-12"> 
        <hr /> 
        <span> 
          <strong>Email : </strong>  order@linuxgeekers.com 
        </span> 
        <span> 
          <strong>Phone : </strong>  085729009963 
        </span> 
        <span> 
          <strong>BBM : </strong>  7CB2DABD 
        </span> 
        <hr />
      </div> 
    </div> 
    <div  class="row pad-top-botm client-info"> 
      <div class="col-lg-4 col-md-4 col-sm-6"> 
        <h4><strong>Shipped to</strong></h4> 
        <?php $order = Order::find(2); ?> 
        <strong>{{ $order->shipping->recipient->name }}</strong> <br /> 
        <b>Address :</b> {{ $order->shipping->recipient->address }} <br /> 
        {{ $order->shipping->recipient->city }} , {{ $order->shipping->recipient->province }} <br /> 
        <b>Phone :</b> {{ $order->shipping->recipient->phone }} <br /> 
      </div> 
      <div class="col-lg-4 col-md-4 col-sm-6"> 
        <h4>  <strong>Shippment Info</strong></h4> 
        <b>Courier : {{ strtoupper($order->shipping->courier) }} </b> <br /> 
        <b>Cost :</b> {{ Helpers::rupiah($order->shipping->cost) }} <br /> 
        <b>Weight :</b> (not yeet) <br /> 
      </div> 
      <div class="col-lg-4 col-md-4 col-sm-6"> 
        <h4><strong>Order Details </strong></h4> 
        <b>Total : {{ Helpers::rupiah($order->total + $order->extra_cost + $order->shipping->cost) }} </b> <br />Date :  {{ Helpers::date($order->date) }} <br /> 
        <b>Payment Status :  (not yet) </b> <br /> 
      </div> 
    </div> 
    <div class="row"> 
      <div class="col-lg-12 col-md-12 col-sm-12"> 
        <div class="table-responsive"> 
          <table class="table table-striped table-bordered table-hover"> 
            <thead> 
              <tr> 
                <th>Product</th> 
                <th>Price</th> 
                <th>Qty</th> 
                <th>Size</th> 
                <th>Sub Total</th> 
              </tr> 
            </thead> 
            <tbody> 
              @foreach($order->product as $product) 
              <tr> 
                <td>{{ $product->name }}</td> 
                <td>{{ Helpers::rupiah($product->pivot->price) }}</td> 
                <td>{{ $product->pivot->qty }}</td> 
                <td>{{ $product->pivot->size }}</td> 
                <td>{{ Helpers::rupiah($product->pivot->subtotal) }}</td> 
              </tr> 
              @endforeach 
            </tbody> 
          </table> 
        </div> 
        <hr /> 
        <div class="ttl-amts"> 
          <h5> Sub Total : {{ Helpers::rupiah($order->total) }} </h5> 
        </div> 
        <div class="ttl-amts"> 
          <h5>  Extra Cost : {{ Helpers::rupiah($order->extra_cost) }} </h5> 
        </div> 
        <div class="ttl-amts"> 
          <h5>  Shipping Cost : {{ Helpers::rupiah($order->shipping->cost) }} </h5> 
        </div> 
        <hr /> 
        <div class="ttl-amts"> 
          <h4> <strong>Total : {{ Helpers::rupiah($order->total + $order->extra_cost + $order->shipping->cost) }}</strong> </h4> 
        </div> 
      </div> 
    </div> 
    <div class="row"> 
      <div class="col-lg-4 col-md-4 col-sm-4 text-center"> 
        <img src="" class="col-lg-6 col-md-4 col-sm-4"> <br /> 
        <strong> BCA </strong> 
        <p>0373323672</p> 
        <p>Anugerah Chandra Utama</p> 
      </div> 
      <div class="col-lg-4 col-md-4 col-sm-4 text-center"> 
        <img src="" class="col-lg-6 col-md-4 col-sm-4"> <br /> 
        <strong> Mandiri </strong> 
        <p>137-00-0751607-9</p> 
        <p>Anugerah Chandra Utama</p> 
      </div> 
      <div class="col-lg-4 col-md-4 col-sm-4"> 
        <strong> Catatan: </strong> 
        <small>
          <i>
            <p>Silakan melakukan pembayaran pada salah satu bank di samping ini.</p> <p>Setelah melakukan pembayaran silakan konfirmasi pembayaran Anda pada link berikut ini.</p> 
          </i>
        </small> 
      </div> 
    </div> 
  </div>
</body>
</html>
