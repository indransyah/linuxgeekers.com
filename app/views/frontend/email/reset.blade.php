<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset</h2>

		<div>
			To reset your password, please visit this link: {{ URL::action('UserMemberController@getValidate', $resetCode) }}.<br/>
		</div>
	</body>
</html>
