@extends('frontend.layouts.master')
@section('content')
	@if(Carousel::take(5)->get()->count() != 0)
	<div class="banner">
    	<div id="fwslider">
        	<div class="slider_container">
        		@foreach(Carousel::take(5)->get() as $image)
            	<div class="slide"> 
					<img src="{{ $image->carousel->url() }}" class="img-responsive" alt=""/>
            	</div>
            	@endforeach
        	</div>
        	<div class="timers"></div>
	        <div class="slidePrev"><span></span></div>
	        <div class="slideNext"><span></span></div>
		</div>
	</div>
	@else
	<div class="banner">
    	<div id="fwslider">
        	<div class="slider_container">
            	<div class="slide"> 
					<img src="{{ URL::to('images/default-carousel.png') }}" class="img-responsive" alt=""/>
            	</div>
        	</div>
        	<div class="timers"></div>
	        <div class="slidePrev"><span></span></div>
	        <div class="slideNext"><span></span></div>
		</div>
	</div>
	@endif
	<div class="features">
		@if(Product::featured()->get()->count() >= 1)
		<div class="container">
			<h3 class="m_3">Features</h3>
			<div class="row">
				@foreach(Product::featured()->get() as $product)
				<div class="col-md-3 top_box">
					<div class="view view-ninth">
						<a href="{{ URL::action('UserProductController@getShow', $product->slug) }}">
							<img src="{{ ($product->picture->count() == 0) ? URL::to('images/default-product.jpg') : $product->picture->first()->photo->url('medium') }}" class="img-responsive" alt=""/>
		                    <!-- <div class="mask mask-1"> </div>
		                    <div class="mask mask-2"> </div>
	                      	<div class="content">
	                        	<h2>Hover Style #9</h2>
	                        	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>
	                      	</div> -->
	                   </a>
					</div>
					<h4 class="m_4"><a href="{{ URL::action('UserProductController@getShow', $product->slug) }}">{{ $product->name }}</a></h4>
					<p class="m_5">{{ Helpers::rupiah($product->price) }}</p>
					<p class="m_5">{{ strtoupper($product->system) }}</p>
            	</div>
            	@endforeach
			</div>
		</div>
		@endif
	</div>
@stop