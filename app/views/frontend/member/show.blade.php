@extends('frontend.layouts.master')
@section('content')

<div class="main">
  <div class="shop_top">
    <div class="container">
      <!-- <div class="row"> -->
        <!-- <div class="col-sm-3 col-md-2"> -->
        <div class="col-lg-3 col-sm-12">
          @include('frontend.member.sidebar')
        </div>
        <!-- <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"> -->
        <div class="col-lg-9 col-sm-12">
          <!-- <h1>Dashboard</h1>
          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxkZWZzLz48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzBEOEZEQiIvPjxnPjx0ZXh0IHg9IjczLjUiIHk9IjEwMCIgc3R5bGU9ImZpbGw6I0ZGRkZGRjtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0O2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjIwMHgyMDA8L3RleHQ+PC9nPjwvc3ZnPg==" data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxkZWZzLz48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzM5REJBQyIvPjxnPjx0ZXh0IHg9IjczLjUiIHk9IjEwMCIgc3R5bGU9ImZpbGw6IzFFMjkyQztmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0O2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjIwMHgyMDA8L3RleHQ+PC9nPjwvc3ZnPg==" data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="200x200">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxkZWZzLz48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzBEOEZEQiIvPjxnPjx0ZXh0IHg9IjczLjUiIHk9IjEwMCIgc3R5bGU9ImZpbGw6I0ZGRkZGRjtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0O2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjIwMHgyMDA8L3RleHQ+PC9nPjwvc3ZnPg==" data-src="holder.js/200x200/auto/sky" class="img-responsive" alt="200x200">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img data-holder-rendered="true" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjxkZWZzLz48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iIzM5REJBQyIvPjxnPjx0ZXh0IHg9IjczLjUiIHk9IjEwMCIgc3R5bGU9ImZpbGw6IzFFMjkyQztmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0O2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjIwMHgyMDA8L3RleHQ+PC9nPjwvc3ZnPg==" data-src="holder.js/200x200/auto/vine" class="img-responsive" alt="200x200">
              <h4>Label</h4>
              <span class="text-muted">Something else</span>
            </div>
          </div> -->

          <h2 class="sub-header">Order Detail</h2>
          @if($invoice->order->status == 'belum dikonfirmasi')
          <div>
            <strong class="pull-right" style="color:red;">{{ strtoupper($invoice->order->status) }}</strong> <br>
            <small class="pull-right"><i>Silakan menunggu konfirmasi biaya pengiriman dari Linux Geekers</i></small>
          </div>
          @else
          <div class="pull-right">
            <a href="{{ URL::action('UserMemberController@getDownload', $invoice->code) }}" class="btn btn-success pull-right" target="_blank">Download Invoice</a><br>
            <small class="pull-right"><i>Silakan melakukan pembayaran. Untuk detail pembayaran dapat dilihat pada Invoice</i></small>
          </div>
          @endif
          <div class="row">
        
        
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-barcode"></i>
                    <h4 class="box-title">Purchased items</h4>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <!-- <dl class="dl-horizontal">
                        <dt>Description lists</dt>
                        <dd>A description list is perfect for defining terms.</dd>
                        <dt>Euismod</dt>
                        <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                        <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                        <dt>Malesuada porta</dt>
                        <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                        <dt>Felis euismod semper eget lacinia</dt>
                        <dd>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>
                    </dl> -->
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Size</th>
                                <th>Subtotal</th>
                            </tr>
                            @foreach($invoice->order->product as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ Helpers::rupiah($product->pivot->price) }}</td>
                                <td>{{ $product->pivot->qty }}</td>
                                <td>{{ strtoupper($product->pivot->size) }}</td>
                                <td>{{ Helpers::rupiah($product->pivot->subtotal) }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">Total</td>
                                <td>{{ Helpers::rupiah($invoice->order->total) }}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-truck"></i>
                    <h4 class="box-title">Shipping Information</h4>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Courier</dt>
                        <dd>{{ strtoupper($invoice->order->shipment->courier) }}</dd>
                        <dt>Recipient Name</dt>
                        <dd>{{ $invoice->order->shipment->recipient->name  }}</dd>
                        <dt>Address</dt>
                        <dd>{{ $invoice->order->shipment->recipient->address  }}</dd>
                        <dt>City</dt>
                        <dd>{{ $invoice->order->shipment->recipient->city }}</dd>
                        <dt>Province</dt>
                        <dd>{{ $invoice->order->shipment->recipient->province }}</dd>
                        <dt>Message</dt>
                        <dd>{{ ($invoice->order->shipment->message) ? $invoice->order->shipment->message : '-' }}</dd>
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->

        @if($invoice->order->status == 'telah dikonfirmasi' && $invoice->payment)
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-shopping-cart"></i>
                    <h4 class="box-title">Payment Information</h4>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Status</dt>
                        <dd><b><p style="color: {{ ($invoice->payment->status == 'lunas')  ? 'green' : 'red' }};">{{ strtoupper($invoice->payment->status) }}</p></b></dd>
                        <dt>Additional Cost</dt>
                        <dd>{{ Helpers::rupiah($invoice->order->extra_cost) }}</dd>
                        <dt>Shipping Cost</dt>
                        <dd>{{ Helpers::rupiah($invoice->order->shipment->cost) }}</dd>
                        <dt>Total</dt>
                        <dd>
                            <strong>{{ Helpers::rupiah($invoice->amount) }}</strong><br />
                        </dd>
                        <dt>Message</dt>
                        <dd>{{ ($invoice->order->message) ? $invoice->order->message : '-' }}</dd>
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->
        @endif

        @if($invoice->order->shipment->status != null)
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-shopping-cart"></i>
                    <h4 class="box-title">Shipment Information</h4>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Status</dt>
                        <dd><b><p style="color: {{ ($invoice->order->shipment->status == 'dikirim')  ? 'green' : 'red' }};">{{ strtoupper($invoice->order->shipment->status) }}</p></b></dd>
                        <dt>Tracking number</dt>
                        <dd>{{ ($invoice->order->shipment->tracking_number) ? $invoice->order->shipment->tracking_number : '-' }}</dd>
                    </dl>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- ./col -->
        @endif
    </div><!-- /.row -->

        </div>
      <!-- </div> -->
    </div>
  </div>
</div>
@stop
