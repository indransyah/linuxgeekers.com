<style type="text/css">
.mysidebar {
  padding: 0px;
}
.mysidebar > li {
    position: relative;
    display: block;
}
.mysidebar > .active > a,
.mysidebar > .active > a:hover,
.mysidebar > .active > a:focus {
  /*border: 1px solid #ddd;*/
  background-color: #000;
  color: #FFF;
}
.mysidebar > li > a {
    position: relative;
    display: block;
    padding: 10px 15px;
    color: #000;
}
.mysidebar > li > a:hover, .mysidebar > li > a:focus {
    text-decoration: none;
    background-color: #000;
    color: #FFF;
}
</style>
<ul class="mysidebar">
	<li class="{{ Request::is('member/history') || Request::is('member/invoice/*') ? 'active' : '' }}"><a href="{{ URL::action('UserMemberController@getHistory') }}"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Order History</a></li>
	<li class="{{ Request::is('member/payment') ? 'active' : '' }}"><a href="{{ URL::action('UserMemberController@getPayment') }}"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span> Confirm Payment</a></li>
</ul>