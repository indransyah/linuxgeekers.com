<html>
<head>
<title>Invoice</title>
<style type="text/css">
  #page-wrap {
    width: 700px;
    margin: 0 auto;
  }
  .center-justified {
    text-align: justify;
    margin: 0 auto;
    width: 30em;
  }
  table.outline-table {
    border: 1px solid;
    border-spacing: 0;
  }
  tr.border-bottom td, td.border-bottom {
    border-bottom: 1px solid;
  }
  tr.border-top td, td.border-top {
    border-top: 1px solid;
  }
  tr.border-right td, td.border-right {
    border-right: 1px solid;
  }
  tr.border-right td:last-child {
    border-right: 0px;
  }
  tr.center td, td.center {
    text-align: center;
    vertical-align: text-top;
  }
  td.pad-left {
    padding-left: 5px;
  }
  tr.right-center td, td.right-center {
    text-align: right;
    padding-right: 50px;
  }
  tr.right td, td.right {
    text-align: right;
  }
  .grey {
    background:grey;
  }
</style>
</head>
<body>
  <div id="page-wrap">
    <table width="100%">
      <tbody>
        <tr>
          <td width="40%">
            <img src="{{ URL::to($company->logo->url('medium')) }}" style="background-color:#000;"> <!-- your logo here -->
          </td>
          <td width="40%">
            <strong>{{ $company->name }}</strong> 
            <br /> 
            <strong>Address :</strong> {{ $company->address }} <br />
            <strong>Email : </strong> {{ $company->email }} <br />
            <strong>Phone : </strong> {{ $company->phone }} <br />
            <strong>BBM : </strong> {{ $company->bbm }}
          </td>
          <td width="20%" style="text-align: center;">
            <h3>Invoice Code</h3>
            <b>{{ strtoupper($invoice->code) }}</b>
            <br />
            <br />
            <br />
          </td>
        </tr>
      </tbody>
    </table>
    <hr />
    <table width="100%">
      <tbody>
        <tr>
          <td width="40%">
            <h3><i>Recipient Detail</i></h3>
              {{ $invoice->order->shipment->recipient->name }}<br>
              {{ $invoice->order->shipment->recipient->phone }}<br>
              {{ $invoice->order->shipment->recipient->address }}<br>
              {{ $invoice->order->shipment->recipient->city }}<br>
              {{ $invoice->order->shipment->recipient->province }}<br>
          </td>
          <td width="30%">
            <h3><i>Shipment Detail</i></h3>
              Courier : {{ strtoupper($invoice->order->shipment->courier) }}<br>
              Cost : {{ Helpers::rupiah($invoice->order->shipment->cost) }}<br>
              <br>
              <br>
              <br>
              <br>
          </td>
          <td width="30%">
            <h3><i>Order Detail</i></h3>
              Total : {{ Helpers::rupiah($invoice->amount) }}<br>
              <br>
              <br>
              <br>
              <br>
              <br>
          </td>
        </tr>
      </tbody>
    </table>
    <br />

    <table width="100%" class="outline-table">
      <tbody>
        <tr class="border-right grey center">
          <td>Product</td>
          <td>Price</td>
          <td>Qty</td>
          <td>Size</td>
          <td>Subtotal</td>
        </tr>
        @foreach($invoice->order->product as $product)
        <tr class="border-bottom border-right">
          <td>{{ $product->name }}</td>
          <td class="right">{{ Helpers::rupiah($product->pivot->price) }}</td>
          <td class="center">{{ $product->pivot->qty }}</td>
          <td class="center">{{ strtoupper($product->pivot->size) }}</td>
          <td class="right">{{ Helpers::rupiah($product->pivot->subtotal) }}</td>
        </tr>
        @endforeach
        <tr>
          <td class="border-bottom right-center border-right" colspan="4">Subtotal</td>
          <td class="border-bottom right">{{ Helpers::rupiah($invoice->order->total) }}</td>
        </tr>
        <tr>
          <td class="border-bottom right-center border-right" colspan="4">Additional Cost</td>
          <td class="border-bottom right">{{ Helpers::rupiah($invoice->order->extra_cost) }}</td>
        </tr>
        <tr>
          <td class="border-bottom right-center border-right" colspan="4">Shipping Cost</td>
          <td class="border-bottom right">{{ Helpers::rupiah($invoice->order->shipment->cost) }}</td>
        </tr>
        <tr>
          <td class="right-center border-right" colspan="4">Total</td>
          <td class="right">{{ Helpers::rupiah($invoice->amount) }}</td>
        </tr>
      </tbody>
    </table>
    <br />

    <table width="100%">
      <tbody>
        <tr>
          <td width="30%" class="center">
            <img src="http://distrokdri.com/assets/images/distrokdri/logo-payment-bca.png"><br>
            <strong> BCA </strong><br>
            0373323672 <br>
            Anugerah Chandra Utama <br>
          </td>
          <td width="30%" class="center">
            <img src="http://distrokdri.com/assets/images/distrokdri/logo-payment-mandiri.png"><br>
            <strong> Mandiri </strong> <br>
            137-00-0751607-9 <br>
            Anugerah Chandra Utama <br>
          </td>
          <td width="40%">
            <strong><i>Catatan</i></strong> <br>
            1. Silakan melakukan pembayaran pada salah satu bank di samping ini. <br>
            2. Setelah melakukan pembayaran silakan konfirmasi pembayaran Anda pada halaman "Member" pada website Linux Geekers <br>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</body>
</html>
