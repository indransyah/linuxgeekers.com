@extends('frontend.layouts.master')
@section('content')
<div class="main">
	<div class="shop_top">
		<div class="container"> 
			@include('frontend.layouts.alert')
			<!-- @if (Session::has('error'))
			<div class="alert alert-danger">
				{{ Session::get('error') }}
				{{ HTML::ul($errors->all()) }}
			</div>
			@endif -->
			{{ Form::open(array('action' => 'UserMemberController@postRegister')) }}
				<div class="register-top-grid">
					<h3>PERSONAL INFORMATION</h3>
					<div>
						<span>Full Name<label>*</label></span> 
						{{ Form::text('name', null, array('placeholder' => 'Nama lengkap Anda', 'type' => 'text', 'required' => 'true' )) }}
						<!-- <input type="text" name="name" required="true">  -->
					</div>
					<div>
						<span>Email Address<label>*</label></span>
						{{ Form::text('email', null, array('placeholder' => 'Alamat email Anda', 'type' => 'text', 'required' => 'true' )) }}
						<!-- <input type="text" name="email" required="true">  -->
					</div>
					<div class="clear"> </div>
				</div>
				<div class="clear"> </div>
				<div class="register-bottom-grid">
					<h3>LOGIN INFORMATION</h3>
					<div>
						<span>Password<label>*</label></span>
						<input type="password" name="password" required="true">
					</div>
					<div>
						<span>Confirm Password<label>*</label></span>
						<input type="password" name="password_confirmation" required="true">
					</div>
					<div class="clear"> </div>
				</div>
				<div class="clear"> </div>
				<button type="submit" class="button-black">Register</button>
			{{  Form::close() }}
		</div>
	</div>
</div>
@stop