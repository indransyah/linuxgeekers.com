@extends('frontend.layouts.master')
@section('content')
	<div class="main">
    	<div class="shop_top">
			<div class="container">
				<div class="col-md-6">
      				@include('frontend.layouts.alert')
					<div class="login-title">
	           			<h4 class="title">Reset Password</h4>
						<div id="loginbox" class="loginbox">
							{{ Form::open(array('action' => 'UserMemberController@postReset', 'id' => 'login-form')) }}
								<fieldset class="input">
								    <p id="login-form-username">
								    	<label for="modlgn_username">Email</label>
                        				{{ Form::text('email', null, array('class' => 'inputbox', 'type' => 'text', 'size' => '18' )) }}
								    </p>
								    <p></p>
								    <div class="remember">
										<input type="submit" name="Submit" class="button" value="Reset"><div class="clear"></div>
									</div>
								</fieldset>
							{{ Form::close() }}
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
@stop